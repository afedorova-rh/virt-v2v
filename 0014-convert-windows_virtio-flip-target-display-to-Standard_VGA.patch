From 6fad666b1821a7c957a3f1761f95319052075c0b Mon Sep 17 00:00:00 2001
From: Laszlo Ersek <lersek@redhat.com>
Date: Tue, 9 Nov 2021 11:02:57 +0100
Subject: [PATCH] convert/windows_virtio: flip target display to Standard_VGA

Policy change: pick Standard_VGA over either QXL or Cirrus for the video
type in the output (Windows) domain, always.

If the subject Windows version is entirely unsupported by
"virtio-win.iso", continue warning the user, as IDE and RTL8139 are still
considered inferior to virtio-blk and virtio-net, respectively.

No warning is needed when only the QXL driver is missing for the subject
Windows version, as Standard_VGA is not worse than QXL.

Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=1961107
Signed-off-by: Laszlo Ersek <lersek@redhat.com>
---
 convert/windows_virtio.ml | 18 ++----------------
 tests/test-v2v-i-ova.xml  |  2 +-
 2 files changed, 3 insertions(+), 17 deletions(-)

diff --git a/convert/windows_virtio.ml b/convert/windows_virtio.ml
index 25b455281bd2..02ef4a2cbca3 100644
--- a/convert/windows_virtio.ml
+++ b/convert/windows_virtio.ml
@@ -53,7 +53,7 @@ let rec install_drivers ((g, _) as reg) inspect =
       warning (f_"there are no virtio drivers available for this version of Windows (%d.%d %s %s).  virt-v2v looks for drivers in %s\n\nThe guest will be configured to use slower emulated devices.")
               inspect.i_major_version inspect.i_minor_version inspect.i_arch
               inspect.i_product_variant virtio_win;
-      (IDE, RTL8139, Cirrus, false, false, false, false)
+      (IDE, RTL8139, Standard_VGA, false, false, false, false)
   )
   else (
     (* Can we install the block driver? *)
@@ -103,27 +103,13 @@ let rec install_drivers ((g, _) as reg) inspect =
       else
         Virtio_net in
 
-    (* Can we install the QXL driver? *)
-    let video : guestcaps_video_type =
-      let has_qxl =
-        g#exists (driverdir // "qxl.inf") ||
-        g#exists (driverdir // "qxldod.inf") in
-      if not has_qxl then (
-        warning (f_"there is no QXL driver for this version of Windows (%d.%d %s).  virt-v2v looks for this driver in %s\n\nThe guest will be configured to use a basic VGA display driver.")
-                inspect.i_major_version inspect.i_minor_version
-                inspect.i_arch virtio_win;
-        Cirrus
-      )
-      else
-        QXL in
-
     (* Did we install the miscellaneous drivers? *)
     let virtio_rng_supported = g#exists (driverdir // "viorng.inf") in
     let virtio_ballon_supported = g#exists (driverdir // "balloon.inf") in
     let isa_pvpanic_supported = g#exists (driverdir // "pvpanic.inf") in
     let virtio_socket_supported = g#exists (driverdir // "viosock.inf") in
 
-    (block, net, video,
+    (block, net, Standard_VGA,
      virtio_rng_supported, virtio_ballon_supported, isa_pvpanic_supported, virtio_socket_supported)
   )
 
diff --git a/tests/test-v2v-i-ova.xml b/tests/test-v2v-i-ova.xml
index 30f52f557d9f..d7383905fdc0 100644
--- a/tests/test-v2v-i-ova.xml
+++ b/tests/test-v2v-i-ova.xml
@@ -39,7 +39,7 @@
       <model type='virtio'/>
     </interface>
     <video>
-      <model type='qxl' ram='65536' heads='1'/>
+      <model type='vga' vram='16384' heads='1'/>
     </video>
     <graphics type='vnc' autoport='yes' port='-1'/>
     <rng model='virtio'>
-- 
2.19.1.3.g30247aa5d201

